.. image:: https://assets.gitlab-static.net/mayan-edms/api-examples/raw/master/_static/mayan_labs_logo.png
   :alt: Mayan Labs logo

This repository is part of the Mayan Labs projects.

These are projects that are not yet complete or were created for a specific
use. These are early releases.

Experience with Mayan EDMS and programming concepts are required to use
Mayan Labs projects.

Mayan Labs projects are not General Availability and are only
supported via the commercial support offerings (https://www.mayan-edms.com/support/).


===========
Description
===========

Geany theme used by the Mayan EDMS core team.


=======
License
=======

This project is free open source under the `Apache 2.0 License`_.

.. _`Apache 2.0 License`: https://gitlab.com/mayan-edms-geany-theme/importer/raw/master/LICENSE


============
Installation
============

Copy ``colorschemes/mayan.conf`` to ``~/.config/geany/colorschemes``

============
Requirements
============

- **Geany** (https://www.geany.org/)


=======
Credits
=======
Beaker icon by https://www.flaticon.com/authors/bqlqn



